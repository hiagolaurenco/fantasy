import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';


export interface Usuario {
  email: string
  name: string
  password: any
  escalacao: any
}

export interface LigaOficial {
  nome: String
  ligaOficial: any
  participantes: []
}

export interface Liga {
  id: any
  usuarioEmitente: any
  nomeLiga: String
  ligaOficial: any
  participantes: any
}

@Component({
  selector: 'app-dialog-competicao-new',
  templateUrl: './dialog-competicao-new.component.html',
  styleUrls: ['./dialog-competicao-new.component.css']
})
export class DialogCompeticaoNewComponent implements OnInit {
  user: any;
  convidados = [] as any;
  convidado: any;;
  item: any;
  ligasOficiais = [];
  txtNomeLiga: '' | undefined;
  ligaSelected: any;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  @Output() onOk = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<DialogCompeticaoNewComponent>,
    public dialog: MatDialog,
    private competicaoService: CompeticaoService,
    private ligaOficialService : LigaOficialService,
    private usuarioService: UsuarioService,
    private ligaService: LigaService
  ) { }

  usuario: Usuario = {
    email: null,
    name: null,
    password: null,
    escalacao: null
  }

  ligaOficial: LigaOficial = {
    nome : null,
    ligaOficial: null,
    participantes: []
  }

  liga: Liga = {
    id: null,
    usuarioEmitente: null,
    nomeLiga: null,
    ligaOficial: null,
    participantes: [] = [],
  }

  ngOnInit() {
    this.ligaOficialService.listAll().subscribe(r => {
      this.ligasOficiais = r;
    });

  }

  convidarUsuario(){

  }

  criarLiga(){
    console.log("aqui" + this.liga)
    this.liga.usuarioEmitente = localStorage.getItem("login");
    if(this.liga.usuarioEmitente !=null && this.liga.nomeLiga != null && this.liga.ligaOficial != null && this.liga.participantes != null){
      this.ligaService.save(this.liga).subscribe((r: any) => {
        console.log(r);

      }, erro => console.log(erro))
    }else {
      console.log("todos os campos são necessários")
    }
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our invited
    if ((value || '').trim()) {
      console.log(value);
      this.convidados.push({name: value.trim()});
      console.log(this.convidados);
      this.liga.participantes = this.convidados;

    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(convidado: any): void {
    const index = this.convidados.indexOf(convidado);

    if (index >= 0) {
      this.convidados.splice(index, 1);
    }
  }

  setLigaOficial(ligaOficial: string | undefined){
    console.log("aqui" + ligaOficial);
    this.liga.ligaOficial = ligaOficial;
    console.log(this.liga.ligaOficial);
    this.txtNomeLiga = ligaOficial;
//    this.flagEmitente = false;
  }

  select(item) {
    this.onOk.emit({ obj: item.nfeDownloadPropriedade, dialog: this.dialogRef });
  }

  cancelar() {
    this.dialogRef.close();
  }
}
