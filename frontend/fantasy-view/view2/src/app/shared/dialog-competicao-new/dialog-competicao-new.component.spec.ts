import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCompeticaoNewComponent } from './dialog-competicao-new.component';

describe('DialogCompeticaoNewComponent', () => {
  let component: DialogCompeticaoNewComponent;
  let fixture: ComponentFixture<DialogCompeticaoNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogCompeticaoNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCompeticaoNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
