import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

const SERVICE = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router){}

  getToken(credential: any): Observable<any> {
      return this.http.post(`${SERVICE}/login/logar`, credential);
  }

  alterarSenha(alterarSenhaDto: any): Observable<any> {
      return this.http.post(`${SERVICE}/integracao/changePassword`, alterarSenhaDto);
  }

}
