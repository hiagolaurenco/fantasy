import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

const SERVICE = environment.apiUrl;

@Injectable({
    providedIn: 'root'
})
export class JogadorService {

    constructor(private http: HttpClient,  private router: Router) { }

    public listAll() :Observable<any> {
        return this.http.get(`${SERVICE}/jogador`);
    }
    
}
