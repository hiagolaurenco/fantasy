import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

const SERVICE = environment.apiUrl;

@Injectable({
    providedIn: 'root'
})
export class EscalacaoService {

    constructor(private http: HttpClient,  private router: Router) { }

    public salvarEscalacao(escalacao, name) :Observable<any> {
        console.log("aqui");
        console.log(escalacao, name);
        return this.http.post(`${SERVICE}/escalacao/${name}`, escalacao);
    }
    
}
