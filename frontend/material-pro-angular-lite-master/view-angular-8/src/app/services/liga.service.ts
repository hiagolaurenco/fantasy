import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

const SERVICE = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class LigaService {

  constructor(private http: HttpClient, private router: Router){}

  save(liga): Observable<any> {
    console.log(liga);
      return this.http.post(`${SERVICE}/liga/save/`, liga);
  }

}
