import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

const SERVICE = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CompeticaoService {

  constructor(private http: HttpClient, private router: Router){}

  getUser(login: any): Observable<any> {
      return this.http.get(`${SERVICE}/user/find-by-name/${login}`);
  }

}
