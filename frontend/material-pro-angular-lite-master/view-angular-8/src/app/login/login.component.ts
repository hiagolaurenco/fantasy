import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';


 const email = new FormControl('', Validators.required);
 const password = new FormControl('', Validators.required);

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

   public form: FormGroup;
   btnLabel = 'Login';

  constructor(
    private loginService: LoginService,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog) { }

  ngOnInit() {
    email.setValue(localStorage.getItem('email'));
    localStorage.clear();
    this.form = this.fb.group({
      email: email,
      password: password
    });
  }

  logar() {
    this.btnLabel = 'Logando...';
    this.loginService.getToken(this.form.value).subscribe(ret => {
      this.btnLabel = 'Login';
      if(ret.error != null) {
        console.log(ret.error);
        localStorage.clear();
        if (ret.error == 'unauthorized') {
          console.log("Usuário ou Senha incorreta!")
          // this.alert.openError("Usuário ou Senha incorreta!");
          return;
        }
        console.log(ret.error)
        // this.alert.openError(ret.error);
        this.router.navigate(['/register']);
      } else {
        if(ret.access_token) {
          localStorage.setItem('access_token', ret.access_token);
          localStorage.setItem('nome', ret.nome);
          localStorage.setItem('admin', ret.admin);
          localStorage.setItem('sobrenome', ret.extra.sobrenome);
          localStorage.setItem('login', ret.login);
          this.router.navigate(['/']);
        } else {
          console.log(ret);
          // this.alert.openError("Não foi possível fazer Login, tente novamente mais tarde ou entre em contato com o suporte.");
        }
      }
    }, error => {
      this.btnLabel = 'Login';
      console.log(error);
      // this.alert.openError("Não foi possível fazer Login, tente novamente mais tarde ou entre em contato com o suporte.");
    });
  }

}
