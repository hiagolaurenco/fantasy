import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { EscalacaoService } from '../services/escalacao.service';
import { JogadorService } from '../services/jogador.service';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-escalacao',
  templateUrl: './escalacao.component.html',
  styleUrls: ['./escalacao.component.css']
})
export class EscalacaoComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  table: MatTableDataSource<any> = new MatTableDataSource([]);

  displayedColumns = ['imagemTime', 'jogador', 'preco', 'rating', 'acoes'];
  time: any = [];
  showImages: Boolean[] = [false, false, false, false, false];
  imagemTime = '';
  showImage1 = false;
  saldo = 100.00;
  precoTime = 0;
  timeCompleto = false;
  jogadorFoiEscalado = false;
  i = 0;
  usuario: any;

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private jogadorService: JogadorService,
    private escalacaoService: EscalacaoService,
    private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.table.paginator = this.paginator;
    this.listAll();
    console.log("SALDO : ", this.saldo);
    console.log("PREÇO TIME: ", this.precoTime)
    this.verificaEscalacaoExiste();
    // this.teste();
  }

  verificaEscalacaoExiste() {
    let login = localStorage.getItem("login");
    this.usuarioService.getUser(login).subscribe(result => {
      console.log(result);
      if(result != null){
        result.escalacao.jogadores.forEach(element => {
          this.time.push(element);
          this.showImages[this.i] = true;
          this.i++;
        });
      }
    });
  }

  listAll() {
    this.jogadorService.listAll().subscribe(r => {
      this.table.data = r;
      console.log(r);
    });
  }

  addJogadorTime(jogador) {
    if (this.saldo < jogador.preco) {
      console.log("SALDO INSUFICIENTE ! ");
      return null;
    }
    if (this.time.length > 4) {
      this.timeCompleto = true;
      console.log("ESCALAÇÃO COMPLETA ! ");
      return null;
    }
    console.log("VALOR I: ", this.i);
    if (this.i == 0) {
      this.time[this.i] = jogador;
      this.showImages[this.i] = true;
      this.precoTime = this.precoTime + jogador.preco;
      this.saldo = this.saldo - jogador.preco;
      this.i++;
    } else {
      this.verifica(jogador);
    }
  }

  verifica(jogador) {
    this.time.forEach(element => {
      if (jogador.id == element.id) {
        this.jogadorFoiEscalado = true;
      }
    });
    if (!this.jogadorFoiEscalado) {
      this.time[this.i] = jogador;
      this.showImages[this.i] = true;
      this.precoTime = this.precoTime + jogador.preco;
      this.saldo = this.saldo - jogador.preco;
      this.i++;
    } else {
      console.log("JOGAODR JÁ ESCALADO !!!! ");
      this.jogadorFoiEscalado = false;
    }

  }

  confirmarEscalacao() {
    if(this.time.length != 5){
      console.log("ESCALAÇÃO INCOMPLETA");
    }else{
      let name =  localStorage.getItem("login"),
      escalacao = {
        jogadores : this.time,
        patrimonio : this.saldo + this.precoTime
      }
      this.escalacaoService.salvarEscalacao(escalacao, name).subscribe(ret => {
        console.log("retorno do metodo salvar escalação : " , ret);
      });
      console.log("CONFIRMANDO ESCALAÇÃO")
      console.log(this.time);
    }

  }

  venderTime() {

  }

  moverTela(element) {
    console.log(element)
    element.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" })
  }

  // teste() {
  //   for (let i = 0; i <= this.time.length; i++) {
  //     console.log("time", this.time[i].nome);
  //   }

  //   for (let i = 0; i <= this.showImages.length; i++) {
  //     console.log("showImagem", i);
  //   }

  // }
}
