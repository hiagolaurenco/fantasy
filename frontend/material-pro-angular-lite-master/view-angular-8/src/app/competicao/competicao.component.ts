
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DialogCompeticao } from '../shared/dialog-competicao-new/dialog-competicao';

@Component({
  selector: 'app-competicao',
  templateUrl: './competicao.component.html',
  styleUrls: ['./competicao.component.css']
})
export class CompeticaoComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  criarLiga(){
    const dialogRef = this.dialog.open(DialogCompeticao, {
      height: 'auto',
      minWidth: '40%'
    });
    dialogRef.componentInstance.user;
  }

}
