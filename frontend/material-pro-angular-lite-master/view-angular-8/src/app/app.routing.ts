import { Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { CompeticaoComponent } from './competicao/competicao.component';
import { EscalacaoComponent } from './escalacao/escalacao.component';
import { AppBlankComponent } from './layouts/app-blank/app-blank.component';

import { FullComponent } from './layouts/full/full.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'escalacao',
        component: EscalacaoComponent,
      },
      {
        path: 'competicao',
        component: CompeticaoComponent,
      },
      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
    ]
  },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent}
];
