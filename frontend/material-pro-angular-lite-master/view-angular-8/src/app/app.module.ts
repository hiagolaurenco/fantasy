import { CommonModule, LocationStrategy, PathLocationStrategy } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatIconModule, MatInputModule, MatTreeModule } from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { AppRoutes } from "./app.routing";
import { AuthGuard } from "./auth.guard";
import { CompeticaoComponent } from "./competicao/competicao.component";
import { DashboardModule } from "./dashboard/dashboard.module";
import { DemoMaterialModule } from "./demo-material-module";
import { EscalacaoComponent } from "./escalacao/escalacao.component";
import { AuthInterceptor } from "./http-client-interceptor";
import { AppBlankComponent } from "./layouts/app-blank/app-blank.component";
import { FullComponent } from "./layouts/full/full.component";
import { AppHeaderComponent } from "./layouts/full/header/header.component";
import { AppSidebarComponent } from "./layouts/full/sidebar/sidebar.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { CompeticaoService } from "./services/competicao.service";
import { EscalacaoService } from "./services/escalacao.service";
import { JogadorService } from "./services/jogador.service";
import { LigaService } from "./services/liga.service";
import { LigaOficialService } from "./services/ligaOficial.service";
import { LoginService } from "./services/login.service";
import { UsuarioService } from "./services/usuario.service";
import { SharedModule } from "./shared/shared.module";
import { SpinnerComponent } from "./shared/spinner.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

 
@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    AppHeaderComponent,
    SpinnerComponent,
    AppSidebarComponent,
    EscalacaoComponent,
    LoginComponent,
    RegisterComponent,
    AppBlankComponent,
    CompeticaoComponent
  ],
  imports: [    
    DemoMaterialModule,
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    SharedModule,
    DashboardModule,
    MatInputModule,
    MatTreeModule,
    ReactiveFormsModule,
    MatDialogModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true

    },
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    },
    AuthGuard,
    LoginService,
    JogadorService,
    EscalacaoService,
    UsuarioService,
    CompeticaoService,
    LigaOficialService,
    LigaService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
