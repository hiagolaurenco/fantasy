import * as sha256 from "sha256";
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { RegisterService } from "../services/register.service";
import { MatDialog } from "@angular/material/dialog";

// const name = new FormControl('', Validators.required);
// const email = new FormControl('', Validators.required);
// const password = new FormControl('', Validators.required);
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public form: FormGroup

  cadastroDto: any = {
    usuario: {
      name: '',
      email: '',
    },
    user: {
      password: '',
      login: '',
      roles:[],
      extra: {
        nome: '',
        sobrenome: '',
      }
    }
  };

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private registerService: RegisterService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.required],
      password: [null, Validators.required] ,
      confirmPassword: [null, Validators.required],
    })
  }

  cadastrar() {

    let hash = sha256(this.form.value.password);
    let confhash = sha256(this.form.value.confirmPassword);
    let nome = this.form.value.name.split(' ');
    if (hash == confhash) {
      this.cadastroDto.usuario.name = this.form.value.name;
      this.cadastroDto.usuario.email = this.form.value.email;

      this.cadastroDto.user.password = this.form.value.password;
      this.cadastroDto.user.login = this.form.value.email;
      this.cadastroDto.user.extra.nome = this.form.value.name;
      this.cadastroDto.user.roles.push("ROLE_USER");
      this.cadastroDto.user.password = hash;

      this.registerService.salvar(this.cadastroDto).subscribe(u => {
        console.log(u);
        let resp = u;
        if (resp == true) {
          console.log(`Parabéns ${nome[0]}! O seu cadastro no e-Extrato foi realizado com sucesso, faça login no sistema`);
          //this.sucesso(`Parabéns ${nome[0]}! O seu cadastrado no e-Extrato foi realizado com sucesso, faça login no sistema!`);
          // this.mensagemService.limparMensagem(8000);
          this.cadastroDto.usuario = '';
          this.form.value.confirmPassword = '';
        } else {
          console.log(`${nome[0]} a chave de acesso informada não é valida, certifique-se e tente novamente`);
          //this.erro(`${nome[0]} a chave de acesso informada não é valida, certifique-se e tente novamente!`);
          //this.mensagemService.limparMensagem(8000);
        }
      });
     console.log(this.form.value);
    } else {
      console.log(`${nome[0]} as senhas informadas não são iguais, certifique-se e tente novamente!`)
      //this.erro(`${nome[0]} as senhas informadas não são iguais, certifique-se e tente novamente!`);
    }

  }

  onSubmit() {
    console.log(this.form.value);
    this.cadastrar();
    this.router.navigate(['/']);
  }

}
