
import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DialogCompeticao } from './dialog-competicao-new/dialog-competicao';
import { TextMaskModule } from 'angular2-text-mask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { DemoMaterialModule } from '../demo-material-module';

@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    DialogCompeticao
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
   ],
   imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    DemoMaterialModule,
    CurrencyMaskModule,
    TextMaskModule,
    FlexLayoutModule
  ],
   entryComponents: [
    DialogCompeticao
   ],
  providers: [ MenuItems ]
})
export class SharedModule { }
