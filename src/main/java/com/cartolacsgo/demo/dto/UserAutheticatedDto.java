package com.cartolacsgo.demo.dto;

import com.cartolacsgo.demo.model.Usuario;

public class UserAutheticatedDto {
	
    private String tipo;
    private String email;
    private String nome;
    private String token;

    public UserAutheticatedDto(String email, String nome, String token, String tipo) {

        this.email = email;
        this.nome = nome;
        this.token = token;
        this.tipo = tipo;
    }

    public UserAutheticatedDto(){}

    public static UserAutheticatedDto toDTO(Usuario usuario, String tipo) {
        return new UserAutheticatedDto(usuario.getEmail(), usuario.getNome(), usuario.getToken(), tipo);
    }

    public String getEmail() {
        return email;
    }

    public String getNome() {
        return nome;
    }

    public String getToken() {
        return token;
    }

    public String getTipo() {
        return tipo;
    }
}
