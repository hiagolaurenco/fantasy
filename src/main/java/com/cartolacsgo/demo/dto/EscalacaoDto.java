package com.cartolacsgo.demo.dto;

import com.cartolacsgo.demo.model.Escalacao;

public class EscalacaoDto {
	
	private String name;

	private Escalacao escalacao;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Escalacao getEscalacao() {
		return escalacao;
	}

	public void setEscalacao(Escalacao escalacao) {
		this.escalacao = escalacao;
	}
	
}
