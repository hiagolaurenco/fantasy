package com.cartolacsgo.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.hibernate.annotations.Type;

@Entity
public class TimeOficial {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String imagemTime;
	
	private Double maps;
	
	private Double kdDiff;
	
	private Double kd;
	
	private Double rating;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getImagemTime() {
		return imagemTime;
	}

	public void setImagemTime(String imagemTime) {
		this.imagemTime = imagemTime;
	}
	
	public Double getMaps() {
		return maps;
	}

	public void setMaps(Double maps) {
		this.maps = maps;
	}

	public Double getKdDiff() {
		return kdDiff;
	}

	public void setKdDiff(Double kdDiff) {
		this.kdDiff = kdDiff;
	}

	public Double getKd() {
		return kd;
	}

	public void setKd(Double kd) {
		this.kd = kd;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Double getRating() {
		return rating;
	}
	
	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "TimeOriginal [id=" + id + ", nome=" + nome + ", imagemTime=" + imagemTime + ", maps=" + maps
				+ ", kdDiff=" + kdDiff + ", kd=" + kd + ", rating=" + rating + "]";
	}
	
}
