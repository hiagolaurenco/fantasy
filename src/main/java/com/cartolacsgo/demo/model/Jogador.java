package com.cartolacsgo.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class Jogador {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false, cascade = {CascadeType.ALL} )
	private TimeOficial time;
	
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String imagemJogadorAvatar;
	
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String imagemJogadorCard;

	private String nome;
	
	private Double preco;
	
	private Boolean status;
	
	private Double kd;

	private Double rating;
	
	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getImagemJogadorAvatar() {
		return imagemJogadorAvatar;
	}

	public void setImagemJogadorAvatar(String imagemJogadorAvatar) {
		this.imagemJogadorAvatar = imagemJogadorAvatar;
	}

	public String getImagemJogadorCard() {
		return imagemJogadorCard;
	}

	public void setImagemJogadorCard(String imagemJogadorCard) {
		this.imagemJogadorCard = imagemJogadorCard;
	}
	
	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	public Double getKd() {
		return kd;
	}

	public void setKd(Double kd) {
		this.kd = kd;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public TimeOficial getTime() {
		return time;
	}

	public void setTime(TimeOficial time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Jogador [id=" + id + ", time=" + time + ", imagemJogadorAvatar=" + imagemJogadorAvatar
				+ ", imagemJogadorCard=" + imagemJogadorCard + ", nome=" + nome + ", preco=" + preco + ", status="
				+ status + ", rating=" + rating + "]";
	}

	

}
