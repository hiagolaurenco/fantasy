package com.cartolacsgo.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Convite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	private Usuario emitente;
	
	//private LigaOficial ligaOficial;
	
	@ManyToOne
	private Usuario destinatario;

	public Long getId() {
		return id;
	}

	public Usuario getEmitente() {
		return emitente;
	}

	public void setEmitente(Usuario emitente) {
		this.emitente = emitente;
	}

	public Usuario getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Usuario destinatario) {
		this.destinatario = destinatario;
	}
	
//	public LigaOficial getLigaOficial() {
//		return ligaOficial;
//	}
//
//	public void setLigaOficial(LigaOficial ligaOficial) {
//		this.ligaOficial = ligaOficial;
//	}







	
	

}
