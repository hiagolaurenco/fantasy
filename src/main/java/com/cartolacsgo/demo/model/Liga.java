package com.cartolacsgo.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter 
@ToString
public class Liga {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Getter
	@Setter 
	private String usuarioEmitente;
	
	private String nomeLiga;
	
	@ManyToOne
	private LigaOficial ligaOficial;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	private List<Usuario> participantes = new ArrayList<>();

	
}
