package com.cartolacsgo.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class LigaOficial {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	private Double premio;
	
	private String status; //se vai ser online ou na lan
	
	@ManyToMany
	private List<TimeOficial> listTimeOficial = new ArrayList<TimeOficial>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getPremio() {
		return premio;
	}

	public void setPremio(Double premio) {
		this.premio = premio;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<TimeOficial> getListTimeOriginal() {
		return listTimeOficial;
	}

	public void setListTimeOriginal(List<TimeOficial> listTimeOriginal) {
		this.listTimeOficial = listTimeOriginal;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "LigaOficial [id=" + id + ", nome=" + nome + ", premio=" + premio + ", status=" + status
				+ ", listTimeOriginal=" + listTimeOficial + "]";
	}
	

	

}
