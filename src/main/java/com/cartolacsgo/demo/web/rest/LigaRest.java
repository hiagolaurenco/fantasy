package com.cartolacsgo.demo.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.bo.LigaBo;
import com.cartolacsgo.demo.dao.LigaDao;
import com.cartolacsgo.demo.model.Liga;
import com.cartolacsgo.demo.model.Usuario;

@RestController
@RequestMapping("/rest/liga")
public class LigaRest {

	@Autowired
	private LigaDao ligaDao;
	
	@Autowired
	private LigaBo ligaBo;
//	
//	@PostMapping("/save")
//	public void save(@RequestBody Liga liga) {
//		System.out.println(liga.toString());
//		ligaBo.processar(liga);
//		//ligaDao.save(liga);
//	}
}
