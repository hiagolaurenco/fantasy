package com.cartolacsgo.demo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.dao.JogadorDao;
import com.cartolacsgo.demo.model.Jogador;

@RestController
@RequestMapping("/rest/jogador")
public class JogadorRest {
	
	@Autowired
	private JogadorDao jogadorDao;
	
	@GetMapping
	public List<Jogador> get() {
		return jogadorDao.findAll();
	}

}
