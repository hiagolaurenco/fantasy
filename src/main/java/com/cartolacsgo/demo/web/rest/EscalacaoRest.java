package com.cartolacsgo.demo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.dao.EscalacaoDao;
import com.cartolacsgo.demo.dao.UsuarioDao;
import com.cartolacsgo.demo.model.Escalacao;
import com.cartolacsgo.demo.model.Usuario;

@RestController
@RequestMapping("/rest/escalacao")
public class EscalacaoRest {
	
	@Autowired
	private EscalacaoDao escalacaoDao;
	
	@GetMapping
	public List<Escalacao> get() {
		return escalacaoDao.findAll();
	}
	
//	@PostMapping("/{name}")
//	public void save(@PathVariable String nome, @RequestBody Escalacao escalacao) {
//		Usuario usuario = usuarioDao.findByName(nome);
//		usuario.setEscalacao(escalacao);		
//		usuarioDao.save(usuario);
//		System.out.println(usuario.getEscalacao().getJogadores().size());
//		System.out.println(usuario.getNome());
//		
//	}

}
