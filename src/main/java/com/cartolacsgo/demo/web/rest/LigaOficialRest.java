package com.cartolacsgo.demo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.dao.LigaOficialDao;
import com.cartolacsgo.demo.model.LigaOficial;

@RestController
@RequestMapping("/rest/ligaOficial")
public class LigaOficialRest {
	
	@Autowired
	LigaOficialDao ligaOficialDao;
	
	@GetMapping
	public List<LigaOficial> get() {
		try {
			return ligaOficialDao.findAll();			
		} catch (Exception e) {
			return null;
		}
	}

}
