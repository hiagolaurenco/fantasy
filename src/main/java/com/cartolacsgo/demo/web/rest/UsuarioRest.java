package com.cartolacsgo.demo.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.dao.UsuarioDao;
import com.cartolacsgo.demo.model.Usuario;

@RestController
@RequestMapping("/rest/user")
public class UsuarioRest {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@PostMapping("/salvar")
	public void post(@RequestBody Usuario user) {
		usuarioDao.save(user);
	}
	
//	@PostMapping("/cadastrar")
//	public Map<String,Object> post(@RequestBody CadastroDto cadastroDto) {
//		System.out.println("rest usuario ");
//		return usuarioBo.cadastrar(cadastroDto);
//	}
	
	@GetMapping("/find-by-login/{login}")
	public Usuario findByLogin(@PathVariable String login) {
		try {
			return usuarioDao.findByEmail(login);	
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
		
	}

	


}
