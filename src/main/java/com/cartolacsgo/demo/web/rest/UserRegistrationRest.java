package com.cartolacsgo.demo.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.bo.UserRegistrationBo;
import com.cartolacsgo.demo.dto.UserAutheticatedDto;
import com.cartolacsgo.demo.dto.UserRegistrationDto;
import com.cartolacsgo.demo.model.Usuario;

@RestController
@RequestMapping("rest/usuario/")
public class UserRegistrationRest {

	   private UserRegistrationBo userRegistrationBo;

	    @Autowired
	    public void UserRegistrationController(UserRegistrationBo userRegistrationBo){
	        this.userRegistrationBo = userRegistrationBo;
	    }

	    public UserRegistrationRest(){

	    }

	    @PostMapping("/cadastrar")
	    public ResponseEntity<UserAutheticatedDto> registrate(@RequestBody UserRegistrationDto userRegistrationDto){
	        Usuario usuario = userRegistrationBo.registrate(userRegistrationDto.toUser());
	        return  new ResponseEntity<UserAutheticatedDto>(UserAutheticatedDto.toDTO(usuario, "Bearer "), HttpStatus.CREATED);
	    }
}
