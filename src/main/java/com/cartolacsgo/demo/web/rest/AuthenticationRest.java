package com.cartolacsgo.demo.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cartolacsgo.demo.bo.UserAuthenticationBo;
import com.cartolacsgo.demo.dto.DadosLogin;
import com.cartolacsgo.demo.dto.UserAutheticatedDto;
import com.cartolacsgo.demo.model.Usuario;

@RestController
@RequestMapping("/rest/authentication/")
public class AuthenticationRest {

    private UserAuthenticationBo userAuthenticationBo;

    @Autowired
    public void AuthenticationController(UserAuthenticationBo userAuthenticationBo){
        this.userAuthenticationBo = userAuthenticationBo;
    }

    public AuthenticationRest(){

    }

    @PostMapping("/login")
    public ResponseEntity<UserAutheticatedDto> autenticar(@RequestBody DadosLogin dadosLogin, @RequestHeader String Authorization) throws Exception{
        Usuario usuario = userAuthenticationBo.authenticate(dadosLogin, Authorization);
        return new ResponseEntity<UserAutheticatedDto>(UserAutheticatedDto.toDTO(usuario, "Bearer "), HttpStatus.ACCEPTED);
    }
}
