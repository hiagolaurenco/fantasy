package com.cartolacsgo.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cartolacsgo.demo.model.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, String>{


//   @Autowired
//   private EntityManager em;
   
//   @Transactional
//   public User findByEmail(String email) {
//      User user = em.find(User.class, email);
//      return user;
//   }
   
//   public List<ClienteUsuario> findClientes(User user){
//      Query query = em.createQuery("select distinct c from ClienteUsuario c left join fetch c.roles r where c.usuario = :usuario");
//      query.setParameter("usuario", user);
//      return query.getResultList();
//   }
		public Usuario findByEmail(String email);
		
		//public Usuario findByName (String name);
//   
//   @Query(value = "select distinct c from ClienteUsuario c left join fetch c.roles r where c.usuario = ?1")
//   public List<ClienteUsuario> findClientes(User user);
//   
//   @Query(value = "select u from User u where u.email like CONCAT( '%', ?1, '%') and u.password = ?2")
//   public User findPasswords(String email, String senha);
//   
//   @Query(value = "select u from User u where u.email like CONCAT( '%', ?1, '%')")
//   public User findUser(String email);
}
