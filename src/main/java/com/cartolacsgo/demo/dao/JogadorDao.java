package com.cartolacsgo.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cartolacsgo.demo.model.Jogador;

@Repository
public interface JogadorDao extends JpaRepository<Jogador, Long>{

}
