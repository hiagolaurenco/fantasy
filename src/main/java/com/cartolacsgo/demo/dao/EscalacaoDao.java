package com.cartolacsgo.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cartolacsgo.demo.model.Escalacao;

@Repository
public interface EscalacaoDao extends JpaRepository<Escalacao, Long>{

}
