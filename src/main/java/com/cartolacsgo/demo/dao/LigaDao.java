package com.cartolacsgo.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cartolacsgo.demo.model.Liga;

@Repository
public interface LigaDao extends JpaRepository<Liga, Long>{

}
