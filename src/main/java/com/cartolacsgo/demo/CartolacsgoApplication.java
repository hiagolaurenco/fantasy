package com.cartolacsgo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartolacsgoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartolacsgoApplication.class, args);
	}

}
