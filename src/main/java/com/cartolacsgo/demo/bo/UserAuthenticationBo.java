package com.cartolacsgo.demo.bo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cartolacsgo.demo.dao.UsuarioDao;
import com.cartolacsgo.demo.dto.DadosLogin;
import com.cartolacsgo.demo.model.Usuario;

import io.jsonwebtoken.Claims;

@Service
public class UserAuthenticationBo {

	
	private UsuarioDao usuarioDao;
	private TokenBo tokenBo;
	
	@Autowired
	public UserAuthenticationBo(UsuarioDao usuarioDao, TokenBo tokenBo) {
		this.usuarioDao = usuarioDao;
		this.tokenBo = tokenBo;
	}
	
	public Usuario authenticate(DadosLogin dados, String token) throws Exception {
		Usuario usuario = usuarioDao.findByEmail(dados.getEmail());
		if(dados.getSenha().equals(usuario.getSenha()) && !token.isEmpty() && validate(token)) {	
			return usuario;
		
		}else {
			throw new Exception("Erro");
		}
	}

	private boolean validate(String token) throws Exception {
		try {
			String tokenTratado = token.replace("Bearer ", "");
			Claims claims = tokenBo.decodeToken(tokenTratado);
			
			System.out.println(claims.getIssuer());
			System.out.println(claims.getIssuedAt());
			
			//verifica se o token esta expirado
			
			if(claims.getExpiration().before(new Date(System.currentTimeMillis()))) throw new Exception("Error validade Token");
			System.out.println(claims.getExpiration());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
}
