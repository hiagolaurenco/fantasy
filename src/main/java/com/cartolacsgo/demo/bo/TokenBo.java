package com.cartolacsgo.demo.bo;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.cartolacsgo.demo.model.Usuario;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenBo {
	
	private static final long expirationTime = 1800000;
	private String key = "String Aleatoria Secret ";
	
	public String generateToken(Usuario usuario) {

		return Jwts.builder()
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setSubject("Teste Jwt Api")
				.setExpiration(new Date(System.currentTimeMillis()  + expirationTime))
				.signWith(SignatureAlgorithm.HS256, key)
				.compact();
	}

	public Claims decodeToken(String token) {
		return Jwts.parser()
				.setSigningKey(key)
				.parseClaimsJws(token)
				.getBody();
	}	
}
