package com.cartolacsgo.demo.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cartolacsgo.demo.dao.UsuarioDao;
import com.cartolacsgo.demo.model.Usuario;

@Service
public class UserRegistrationBo {

	
    private UsuarioDao usuarioDao;
    private TokenBo tokenBo;

    @Autowired
    public void UserRegistrationService(UsuarioDao usuarioDao, TokenBo tokenBo){
        this.usuarioDao = usuarioDao;
        this.tokenBo = tokenBo;
    }

    public Usuario registrate(Usuario usuario){
    	usuario.setToken(tokenBo.generateToken(usuario));
    	System.out.println("registrate" + usuario.getSenha() +  " + "  + usuario.getEmail() + usuario.getNome() );
        return usuarioDao.save(usuario);
    }
    
}
